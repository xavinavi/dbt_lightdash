SELECT user_id,
  first_name,
  last_name,
FROM public.users
ORDER BY CAST(user_id AS int) ASC